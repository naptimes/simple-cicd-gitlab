const app = require('../server')
const supertest= require('supertest')
const request= supertest(app)


test('Testing homet page....', async ()=>{
    const res= await request.get('/')
    expect(res.status).toBe(200)
    expect(res.text).toBe('<h1>Home Page</h1>')
})

test('Testing simple api ping....', async ()=>{
    const res= await request.get('/api/ping/pong')
    expect(res.status).toBe(200)
    expect(res.text).toBe('<h1>pong!</h1>')
    
})

test('Testing simple api sayhi....', async ()=>{
    const res= await request.get('/api/sayhi/ali')
    expect(res.status).toBe(200)
    expect(res.body.message).toBe('hi ali')
})