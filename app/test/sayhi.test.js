const greetings= require('../hi')

test('Testing simple function for apps....', ()=>{
    // expected null
    expect(greetings()).toBe()
    expect(greetings('Random')).toBe('hi Random')
})