const express= require("express");
const greetings= require('./hi')

const app= express();

app.get('/', (req, res) =>{
    res.send("<h1>Home Page</h1>");
})

app.get('/api/ping/:testId', (req, res)=>{
    const {testId} = req.params
    res.send(`<h1>${testId}!</h1>`)
    res.end()
})

app.get('/api/sayhi/:random', (req, res)=>{
    const {random} = req.params
    res.json({message:greetings(random)})
    res.end()
})

app.all('*', (req, res)=>{
    res.status(404).send(`<h1>Oops!</h1><p>resource not found</p>`)
    res.end()
})

module.exports= app
