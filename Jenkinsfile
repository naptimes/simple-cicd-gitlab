pipeline {
    agent any
    environment{
        DOCKER_TOKEN= credentials('docker-hub')
        PROD_INSTANCE= credentials('prod-instance')
        STAG_INSTANCE= credentials('stag-instance')
        USER_INSTANCE= credentials('user-instance')
        DOCKER_REPO= 'eekngen/simple-apps'
        GIT_REPO_URL= 'https://gitlab.com/eekngen/simple-cicd-gitlab'
        GIT_DIR_NAME= 'simple-cicd-gitlab'
    }
    stages {
        stage('build') {
            steps{
                sh 'npm --prefix ./app ci'
            }
        }
        stage('testing') {
            steps{
                sh 'npm --prefix ./app test'
            }
        }
        stage('login docker'){
            when{
                anyOf{ 
                    branch "main"
                    branch "stag-*"
                }
            }
            steps{
                sh '''
                    echo $DOCKER_TOKEN_PSW | docker login -u $DOCKER_TOKEN_USR --password-stdin
                '''
            }
        }
        stage('build and push image') {
            when{
                anyOf{ 
                    branch "main"
                    branch "stag-*"
                }
            }
            steps{
                sh '''
                    cd app/ && docker build -t $DOCKER_REPO:latest .
                    docker push $DOCKER_REPO --all-tags
                '''
            }
        }
        stage('deploy stag') {
            when {
                branch "stag-*"
            }
            steps{
                // setup remote env
                sh '''
                    ssh -o StrictHostKeyChecking=no $USER_INSTANCE@$STAG_INSTANCE "export APP_IMAGE_STG=$DOCKER_REPO && if cd ~/$GIT_DIR_NAME; then git pull; else cd ~ && git clone $GIT_REPO_URL && cd $GIT_DIR_NAME; fi && docker-compose -f docker-compose.yml -f docker-compose.stag.yml down && docker-compose -f docker-compose.yml -f docker-compose.stag.yml up -d"
                '''
            }
        }
        stage('deploy prod') {
            when{
                // branch main have merge from stag-*
                branch "main"
            }
            steps{
                sh '''
                ssh -o StrictHostKeyChecking=no $USER_INSTANCE@$PROD_INSTANCE "export APP_IMAGE_PROD=$DOCKER_REPO && if cd ~/$GIT_DIR_NAME; then git pull; else cd ~ && git clone $GIT_REPO_URL && cd $GIT_DIR_NAME; fi && docker-compose -f docker-compose.yml -f docker-compose.prod.yml down && docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d"
                '''
            }
        }
    }
    post{
        always{
            sh 'docker logout'
        }
    }
}